export default function (...filings){
    let status = true;
    for (let i in filings) {
        if (!filings[i]) {
            status = false;
            break;
        }
    }
    return status;
}