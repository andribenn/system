export default {
    alert() {
        const btn1 = document.querySelector('.notification__btn1');
        const btn2 = document.querySelector('.notification__btn2');
        const btnClose = document.querySelector('.notification__close-btn');
        return new Promise((resolve) => {
            btn1.addEventListener('click', () => resolve(true));
            btn2.addEventListener('click', () => resolve(false));
            btnClose.addEventListener('click', () => resolve(false));
        })
    }
}

