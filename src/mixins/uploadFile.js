export default {
  methods: {
    uploadFile(event, data) {
      let d = this;
      if (data === 'photosCar') {
        let checkDuplicate = false;
        if (this.upload[data].length > 0) {
          this.upload[data].forEach(function (item) {
            if (item.photo === event.target.files[0].name || item.name === event.target.files[0].name) {
              checkDuplicate = true;
            }
          })
        }
        if (!checkDuplicate && this.imgURL.length < 4) {
          this.upload[data].push(event.target.files[0]);

          if (event.target.files[0]) {
            let fr = new FileReader();
            fr.addEventListener("load", function () {
              if (d.imgURL.length >= 4) {
                d.imgURL.splice(3, 1);
              }
              d.imgURL.push(fr.result);
            }, false);
            fr.readAsDataURL(event.target.files[0]);
          }
        } else if (this.imgURL.length >= 4) {
          //alert('Only 4 photos');
        }
      }
      if (event.target.files[0] && data === 'photo') {
        let fr = new FileReader();
        fr.addEventListener("load", function () {
          d.imgURL = "url(" + fr.result + ")";
        }, false);
        fr.readAsDataURL(event.target.files[0]);
        this.upload[data] = event.target.files[0];
      }
      if (event.target.files[0] && data !== 'photosCar') {
        this.upload[data] = event.target.files[0];
      }
      this.errors[data] = [];
    },
    removePhoto(index) {
      this.imgURL.splice(index, 1);

      if (this.upload['photosCar'].length > index) {
        this.upload['photosCar'].splice(index, 1);
      }
    },
  }
}
