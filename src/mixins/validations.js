export default {
  data() {
    return {
      errors: {}
    }
  },
  methods: {
    validation(dataInput, dataError, regulations, object) {
      let errors = [];
      regulations.forEach(function (item){
        if (dataInput === undefined || item === 'require' && dataInput.length === 0){
           errors.push('Please fill out this field.');
        }
        if (item === 'english' && (/^[A-Za-z0-9!"№;%:?*()_+@#$^&,. '/-]+$/).test(dataInput) === false){
          errors.push('Only english');
        }
        if (item === 'numbers' && (/^[0-9+./ -]+$/).test(dataInput) === false){
           errors.push('Only numbers');
        }
        if (item === 'email' && (/^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu).test(dataInput) === false){
          errors.push('Only email');
        }
      })
      if (object !== undefined){
        this.errors[object][dataError] = errors
      } else {
        this.errors[dataError] = errors
      }
    },
    checkErrors() {
      let status = false
      let data = this;
      Object.keys(this.errors).forEach(function (item){
        if (typeof data.errors[item] === 'object') {
          Object.keys(data.errors[item]).forEach(function (e){
            if (data.errors[item][e].length > 0) {
              status = true
            }
          })
        } else {
          if (data.errors[item].length > 0) {
            status = true
          }
        }
      })
      return status
    },
    updateErrors() {
      let data = this;
      Object.keys(this.errors).forEach(function (item){
        if (typeof data[item] === 'object') {
          Object.keys(data[item]).forEach(function (e){
            data.errors[item][e] = []
          })
        } else {
          data.errors[item] = []
        }
      })
    }
  }
}