export default {
  methods: {
    normalizeDate(date) {
      let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
      let d = new Date(date);
      let n = d.getDay();
      let i = d.getDate();
      let m = d.getMonth();
      let y = d.getFullYear();
      let h = d.getHours();
      let min = d.getMinutes().toString().padStart(2, '0');
      return days[n] + ' ' + i + '.' + (Number(m) + 1)  + '.' + y + ' ' + Number(h) + ':' + min
    },
  }
}
