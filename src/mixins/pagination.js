export default {
  data() {
    return {
      currentPage: 1,
      arrayPages: [],
      elementsOnPage: 8,
      countPages: 0,
      resultingArray: [],
      showPagination: false,
    }
  },
  methods: {
    setElementsOnPage(count) {
      this.elementsOnPage = count;
    },
    getStatusShowPaginationItem(page) {
      return page === 1 || page === this.countPages || page === this.currentPage || page === this.currentPage + 1 || page === this.currentPage - 1
    },
    getCurrentPageArray(page, array) {
      let newArray = [];
      this.currentPage = page
      let lastIndex = this.elementsOnPage * page
      for (let i = lastIndex; i > lastIndex - this.elementsOnPage; i--){
        if (array[i - 1] !== undefined){
          newArray.push(array[i - 1]);
        }
      }
      this.$router.replace({query: {page: page}})
      this.resultingArray = newArray
    },
    startPagination(array) {
      if (array !== undefined) {
        this.countPages = Math.ceil(array.length / this.elementsOnPage);
        this.getCurrentPageArray(this.getCurrentPage(), array)
        this.showPagination = array.length > this.resultingArray.length;
      }
    },
    getCurrentPage() {
      if (this.$route.query.page !== undefined) {
        return Number(this.$route.query.page)
      } else {
        return 1
      }
    }
  },
  created() {
    this.currentPage = Number(this.$route.query.page)
  }
}
