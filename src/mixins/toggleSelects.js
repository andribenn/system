export default {
  data() {
    return {
      selects: {},
    }
  },
  methods: {
    toggleSelect(select, type, input, array, filteredArray) {
      // pizdec
      if (type === undefined) {
        let status = this.selects[select];
        this.closeAllSelects();
        this.selects[select] = !status;
      } else if (type === 'input' || type === 'focus') {
        if (select === 'serviceClass') {
          this[filteredArray] = array.filter((el) => el.title.toLowerCase().includes(input.toLowerCase()))
        } else if (select === 'year' || select === 'color') {
          this[filteredArray] = array.filter((el) => String(el).toLowerCase().includes(String(input).toLowerCase()))
        } else if (select === 'driver') {
          this[filteredArray] = array.filter((el) => (el.first_name + ' ' + el.last_name).toLowerCase().includes(String(input).toLowerCase()))
        } else if (select === 'vehicle') {
          this[filteredArray] = array.filter((el) => el.licence.toLowerCase().includes(String(input).toLowerCase()))
        } else {
          this[filteredArray] = array.filter((el) => el.name.toLowerCase().includes(input.toLowerCase()))
        }
        // if (type === 'input') {
        //   this[item] = {}
        // }
        this.closeAllSelects();
        this.selects[select] = true;
      }
    },
    blurSelect(input, item, object) {
      if (input === 'serviceClassName') {
        if (item.title !== undefined){
          if (object !== undefined) {
            this[object][input] = item.title
          } else {
            this[input] = item.title
          }
        }
      } else if (input === 'yearInput' || input === 'colorInput') {
        if (String(item) !== undefined){
          if (object !== undefined) {
            this[object][input] = item
          } else {
            this[input] = item
          }
        }
      } else if (item.name !== undefined) {
        if (object !== undefined) {
          this[object][input] = item.name
        } else {
          this[input] = item.name
        }
      }
    },
    closeAllSelects() {
      let selectsData = Object.keys(this.selects);
      let data = this;
      selectsData.forEach(function (item){
        data.selects[item] = false
      })
    },
    closeAllSelectsEvent(e) {
      if (!e.target.classList.contains('select__touch')) {
        this.closeAllSelects()
      }
    },
    changeSelect(data, item, select, object, input) {
      this.selects[select] = false;
      this[input] = item.name
      if (object !== undefined){
        this[object][data] = item;
        if (data === 'country'){
          this[object]['city'] = '';
          if (this.$route.matched[0].path === '/verification') {
            this.getCities({countryId: this[object][data].id, select: select}, object)
          } else {
            this.$emit('getCities', {countryId: this[object][data].id, select: select})
          }
        }
        this.errors[object][data] = []
      } else {
        this[data] = item;
        if (data === 'country'){
          this['city'] = '';
          console.log(this.$route.matched[0])
          if (this.$route.name === 'profile' || this.$route.name === 'company'){
            this.$emit('getCities', {countryId: this[data].id, select: select})
          } else {
            this.getCities({countryId: this[data].id, select: select})
          }
        }
        if (data === 'carMark'){
          this.carModel = '';
          this.getCarModels();
        }
        this.errors[data] = []
      }
      if (select === 'serviceClass'){
        if (item.title === 'Luxury sedan' && this.year < new Date().getFullYear() - 5) {
          this.year = '';
          this.yearInput = '';
        }
        this.getYears();
      }
      if (select === 'driver' || select === 'vehicle') {
        this.updateVehicleAndDriver();
      }
    },
  },
  created() {
    document.addEventListener('click', this.closeAllSelectsEvent)
  },
  beforeUnmount() {
    document.removeEventListener('click', this.closeAllSelectsEvent)
  }
}
