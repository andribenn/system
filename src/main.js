import './static/scss/style.css'

import { createApp } from 'vue'
import App from './App.vue'
import Router from "./router.js";
import Store from "./store.js";
import VueTelInput from 'vue3-tel-input';
import 'vue3-tel-input/dist/vue3-tel-input.css';
import '@vuepic/vue-datepicker/dist/main.css'
const VueTelInputOptions = {
  mode: "international",
  dropdownOptions: {
    separateDialCode: true,
  },
}

createApp(App)
  .use(Router)
  .use(Store)
  .use(VueTelInput, VueTelInputOptions)
  .mount('#app')
