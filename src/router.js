import {createRouter, createWebHistory} from 'vue-router';

import Login from './pages/login/login.vue'
import Verifications from "./pages/verification/index.vue";
import UpdatePassword from "./pages/login/update-password.vue";
import Bookings from "./pages/bookings/index.vue";
import Offers from "./pages/bookings/offers.vue";
import PlannedRides from "./pages/bookings/planned-rides/index.vue";
import PlannedRide from "./pages/bookings/planned-rides/planned-ride.vue";
import FinishedRides from "./pages/bookings/finished-rides/index.vue";
import FinishedRide from "./pages/bookings/finished-rides/finished-ride.vue";
import Vehicles from "./pages/vehicles/index.vue";
import Drivers from "./pages/drivers/index.vue";
import Account from "./pages/account/index.vue";
// import Invoice from "./pages/invoice.vue";
import VehiclesItem from "./pages/vehicles/vehicles-item.vue";
import VehicleAdd from "./pages/vehicles/vehicle-add.vue";
import DriverAdd from "./pages/drivers/driver-add.vue";
import DriversItem from "./pages/drivers/drivers-item.vue";
import Profile from "./pages/account/profile.vue";
import Company from "./pages/account/company.vue";
import NewPassword from "./pages/verification/new-password.vue";
import VerificationProfile from "./pages/verification/profile.vue";
import VerificationDocumentation from "./pages/verification/documentation.vue";
import VerificationCompany from "./pages/verification/company.vue";
import Feedback from "./pages/feedback.vue";
import VerificationBilling from "./pages/verification/billing.vue"
import VerificationAgreement from "./pages/verification/agreement.vue"
import VerificationSuccess from  "./pages/verification/success.vue"


const routes = [
  {
    name: 'login',
    path: '/login',
    meta: { layout: 'LayoutLogin' },
    component: Login
  },
  {
    name: 'verification',
    path: '/verification',
    component: Verifications,
    children: [
      {
        name: 'new-password',
        path: '/verification/new-password',
        meta: { layout: 'LayoutVerification' },
        component: NewPassword,
      },
      {
        name: 'verification-profile',
        path: '/verification/profile',
        meta: { layout: 'LayoutVerification' },
        component: VerificationProfile,
      },
      {
        name: 'verification-documentation',
        path: '/verification/documentation',
        meta: { layout: 'LayoutVerification' },
        component: VerificationDocumentation,
      },
      {
        name: 'verification-company',
        path: '/verification/company',
        meta: { layout: 'LayoutVerification' },
        component: VerificationCompany,
      },
      {
        name: 'verification-billing',
        path: '/verification/billing',
        meta: { layout: 'LayoutVerification' },
        component: VerificationBilling,
      },
      {
        name: 'verification-agreement',
        path: '/verification/agreement',
        meta: { layout: 'LayoutVerification' },
        component: VerificationAgreement,
      },
      {
        name: 'verification-success',
        path: '/verification/success',
        meta: { layout: 'LayoutVerification' },
        component: VerificationSuccess,
      }
    ]
  },
  {
    name: 'update-password',
    path: '/update-password',
    meta: { layout: 'LayoutLogin' },
    component: UpdatePassword
  },
  {
    name: 'bookings',
    path: '/bookings',
    meta: { layout: 'LayoutDefault', title: 'Bookings' },
    component: Bookings,
    children: [
      {
        name: 'offers',
        path: '/bookings/offers',
        meta: { layout: 'LayoutDefault' },
        component: Offers,
      },
      {
        name: 'planned-rides',
        path: '/bookings/planned-rides',
        meta: { layout: 'LayoutDefault' },
        component: PlannedRides,
      },
      {
        name: 'finished-rides',
        path: '/bookings/finished-rides',
        meta: { layout: 'LayoutDefault' },
        component: FinishedRides,
      },
    ]
  },
  {
    name: 'finished-ride',
    path: '/bookings/finished-rides/:id',
    meta: { layout: 'LayoutDefault', title: 'Bookings' },
    component: FinishedRide,
  },
  {
    name: 'planned-ride',
    path: '/bookings/planned-rides/:id',
    meta: { layout: 'LayoutDefault', title: 'Bookings' },
    component: PlannedRide,
  },
  {
    name: 'vehicles',
    path: '/vehicles',
    meta: { layout: 'LayoutDefault', title: 'Vehicles list' },
    component: Vehicles,
  },
  {
    name: 'vehicle',
    path: '/vehicles/:id',
    meta: { layout: 'LayoutDefault', title: 'Vehicle' },
    component: VehiclesItem,
  },
  {
    name: 'vehicle-add',
    path: '/vehicles/add',
    meta: { layout: 'LayoutDefault', title: 'Vehicle' },
    component: VehicleAdd,
  },
  {
    name: 'drivers',
    path: '/drivers',
    meta: { layout: 'LayoutDefault', title: 'Drivers list' },
    component: Drivers,
  },
  {
    name: 'driver',
    path: '/drivers/:id',
    meta: { layout: 'LayoutDefault', title: 'Driver' },
    component: DriversItem,
  },
  {
    name: 'driver-add',
    path: '/drivers/add',
    meta: { layout: 'LayoutDefault', title: 'Driver' },
    component: DriverAdd,
  },
  {
    name: 'account',
    path: '/account',
    meta: { layout: 'LayoutDefault', title: 'Account' },
    component: Account,
    children: [
      {
        name: 'profile',
        path: '/account/profile',
        meta: { layout: 'LayoutDefault' },
        component: Profile,
      },
      {
        name: 'company',
        path: '/account/company',
        meta: { layout: 'LayoutDefault' },
        component: Company,
      },
    ]
  },
  // {
  //   name: 'invoice',
  //   path: '/invoice',
  //   meta: { layout: 'LayoutDefault', title: 'Invoice' },
  //   component: Invoice
  // },
  {
    name: 'feedback',
    path: '/feedback',
    meta: { layout: 'LayoutDefault', title: 'Feedback' },
    component: Feedback
  },
];


const router = createRouter({
  history: createWebHistory(),
  routes,
  mode:'history',
});

export default router;
