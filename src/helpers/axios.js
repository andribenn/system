import axios from 'axios'
import router from '@/router'

const api = axios.create({ baseURL: process.env.VUE_APP_API_DOMAIN })

api.interceptors.request.use((config) => {
	if (config.url.startsWith(process.env.VUE_APP_API_DOMAIN)) {
		config.headers.Authorization = `Bearer ${localStorage.getItem('auth_token')}`
	}
	return config
});

api.interceptors.response.use(
	(response) => response,
	(error) => {
		if (error.response.status === 401 && router.currentRoute.value.path !== '/verification/new-password') {
			localStorage.removeItem('token');
			window.location.href = '/login'; // Redirect to login page
		}
		return Promise.reject(error);
	}
);

export { api }
