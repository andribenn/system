import Vuex from 'vuex'
import {api as axios} from "@/helpers/axios";

export default new Vuex.Store({
  state: {
    user: {
      profile: {},
    },
    edit: false,
    load: false,
    profile: {},
    company: {},
    isShowBooking: false,
    verificationLoadingList: {},
    loadPercent: 0,
  },
  mutations: {
    SET_USER(state, user){
      state.user = user;
    },
    SET_SHOW_BOOKING(state, value) {
      state.isShowBooking = value
    },
    CHANGE_EDIT(state, value){
      state.edit = value;
    },
    CHANGE_STATUS_LOAD(state, value) {
      state.load = value;
    },
    SET_PROFILE(state, profile) {
      state.profile = profile;
    },
    SET_COMPANY(state, company) {
      state.company = company;
    },
  },
  getters: {
    getUser(state) {
      return state.user;
    },
    getStatusEdit(state) {
      return state.edit;
    },
    getStatusLoad(state) {
      return state.load;
    },
    getProfileInSession(state) {
      return state.profile;
    },
    getCompanyInSession(state) {
      return state.company;
    },
  },
  actions: {
    verificationDocumentationQuery({ commit, state }, { formData }) {
      axios.post(`${process.env.VUE_APP_API_DOMAIN}profile/update`, formData,
          {
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            onUploadProgress: (progressEvent) => {
              const percent = Math.round((progressEvent.loaded * 100) / progressEvent.total);
              state.verificationLoadingList = {
                ...state.verificationLoadingList,
                Documentation: percent,
              }
            }
          })
          .then(resp => {
            if (resp.data.status) {
              commit('SET_USER', resp.data.user);
            } else {
              alert(`Error - ${JSON.stringify(resp.data || {})}`)
            }
          })
    },
    verificationCompanyQuery({ commit, state }, { formData, id }) {
      axios.post(`${process.env.VUE_APP_API_DOMAIN}company/${id}`, formData,
          {
            headers: {
              'Content-Type': 'multipart/form-data'
            },
            onUploadProgress: (progressEvent) => {
              const percent = Math.round((progressEvent.loaded * 100) / progressEvent.total);
              state.verificationLoadingList = {
                ...state.verificationLoadingList,
                Company: percent,
              }
            }
          })
          .then(resp => {
            if (resp.data.status) {
              commit('SET_COMPANY', resp.data.company);
              // this.$store.commit('CHANGE_STATUS_LOAD', false);
            }
          })
    },
  }
})
