const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const { VueLoaderPlugin } = require('vue-loader');
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const webpack = require('webpack');
const dotenv = require('dotenv')
const dotenv_webpack = require('dotenv-webpack');

dotenv.config();

module.exports = {
  mode: 'production',
  entry: './src/main.js',
  output: {
    path: path.resolve(__dirname, './dist'),
    filename: 'build.[chunkhash].js',
    publicPath: '/',
  },
  resolve: {
    alias: {
      images: path.resolve(__dirname, './src/static/images/'),
      '@': path.resolve(__dirname, './src'),
    },
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.html$/i,
        loader: "html-loader",
      },
      {
        test: /\.(jpeg|jpg|png|gif|svg)$/i,
        loader: 'file-loader',
        dependency: { not: ['url'] },
        options: {
          name: '[name].[hash:6].[ext]',
          outputPath: 'images',
          publicPath: '/images',
          emitFile: true,
          esModule: false
        },
        type: 'javascript/auto'
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          "style-loader",
          { loader: "css-loader", options: { import: true, url: true } },
          "sass-loader",
        ],
      },
      {
        test    : /\.(png|jpg|svg)$/,
        include : path.join(__dirname, 'img'),
        loader  : 'url-loader'
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, "css-loader"],
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }
      }
    ],
  },
  plugins:[
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].css'
    }),
    new HtmlWebpackPlugin({
      template: './public/index.html',
      favicon: './public/favicon.ico'
    }),
    new CleanWebpackPlugin(),
    new dotenv_webpack(),
    new webpack.DefinePlugin({
      __VUE_OPTIONS_API__: true, __VUE_PROD_DEVTOOLS__: true,
      'process.env.VUE_APP_API_DOMAIN': JSON.stringify(process.env.VUE_APP_API_DOMAIN),
      'process.env.VUE_APP_IMG_LINK': JSON.stringify(process.env.VUE_APP_IMG_LINK)
    })
  ],
};
